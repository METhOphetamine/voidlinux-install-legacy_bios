#!/bin/sh
# Script to be executed while chrooted in the new Void Linux.
# You must be chrooted as root.

DEVICE="$1"
UEFI="$2"
ROOT_PASSWORD="$3"

# Define new root password
# echo "root:${ROOT_PASSWORD}" | chpasswd
passwd
chown root:root /
chmod 755 /

if [ "$UEFI" = "no" ]; then
    mkdir -p /boot
    grub-mkconfig -o /boot/grub/grub.cfg
    grub-install --target=i386-pc "$DEVICE"
else
    mkdir -p /boot/EFI
    grub-mkconfig -o /boot/grub/grub.cfg
    grub-install --target=x86_64-efi --efi-directory=/boot --removable --no-nvram "$DEVICE"
fi
xbps-reconfigure -f glibc-locales

# Finish the install reconfiguring everything
xbps-reconfigure -fa

# Make internet work after reboot (not mandatory)
# On void you do that by enabling the dhcpcd service, which you do by symbolically linking a file. more on: https://docs.voidlinux.org/config/services/index.html
ln -s /etc/sv/dhcpcd /var/service
