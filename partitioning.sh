#!/bin/sh
# Partitioning the requested disk
# The partition table to use is parametered in sfdisk-partitions.dat
# I:
# 1: Device to use, eg. "/dev/sda"
DEVICE="$1"
UEFI="$2"
BOOT_PARTITION_SIZE="$3"

sfdisk --delete "$DEVICE"

if [ "$UEFI" = "no" ]; then
	cat <<EOF | sfdisk "$DEVICE"
label: mbr

size="${BOOT_PARTITION_SIZE}" type="83"
type="83"
EOF
else
	cat <<EOF | sfdisk "$DEVICE"
label: gpt

size="${BOOT_PARTITION_SIZE}" type="C12A7328-F81F-11D2-BA4B-00A0C93EC93B"
type="E6D6D379-F507-44C2-A23C-238F2A3DF928"
EOF
fi
