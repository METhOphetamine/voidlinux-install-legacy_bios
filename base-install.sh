#!/bin/sh
# Basic system installation
# Inputs:
# 1. Device. Eg. "/dev/sda"
DEVICE="$1"
# 2. Volume groupe name. Eg. "void"
VOLUME_GROUP_NAME="$2"
# 3. Hostname.
HOSTNAME="$3"
# 4. Boot partition label. Must be locally unique.
BOOT_PARTITION_LABEL="$4"
UEFI="$5"
ROOT_PASSWORD="$6"

BTRFS_OPTIONS="rw,noatime,compress=zstd,discard=async"

REPO="https://repo-default.voidlinux.org/current/musl"

# Copy the RSA keys from the installation medium to the target root directory:
mkdir -p /mnt/var/db/xbps/keys
cp /var/db/xbps/keys/* /mnt/var/db/xbps/keys/

if [ "${UEFI}" = "no" ]; then
	xbps-install -Sy -R ${REPO} -r /mnt base-system lvm2 cryptsetup grub
else
	# UEFI systems will have a slightly different package selection. The installation command for a UEFI system will be as follows.
	xbps-install -Sy -R ${REPO} -r /mnt base-system cryptsetup grub-x86_64-efi lvm2
fi



# Mount some directories before chrooting
mkdir -p /mnt/dev
mkdir -p /mnt/proc
mkdir -p /mnt/sys
mount -t proc /proc /mnt/proc
mount --rbind /dev /mnt/dev
mount --rbind /sys /mnt/sys

# Configure system files
cp -f rc.conf /mnt/etc/rc.conf

echo "${HOSTNAME}" | tee /mnt/etc/hostname > /dev/null

echo "hostonly=yes" | tee /mnt/etc/dracut.conf.d/hostonly.conf > /dev/null

cp -f grub /mnt/etc/default/grub

cp -f libc-locales /mnt/etc/default/libc-locales

mkdir -p /mnt/boot/grub

# The UUID and PARTUUID is changed on first boot. We use a LABEL.
mkdir -p /mnt/etc
cat <<EOF > /mnt/etc/fstab
/dev/mapper/${VOLUME_GROUP_NAME}-root	/		btrfs	${BTRFS_OPTIONS},subvol=@		0 1
/dev/mapper/${VOLUME_GROUP_NAME}-swap 	none 		swap  	defaults				0 1
/dev/mapper/${VOLUME_GROUP_NAME}-root	/home 		btrfs 	${BTRFS_OPTIONS},subvol=@home		0 2
/dev/mapper/${VOLUME_GROUP_NAME}-root 	/.snapshots	btrfs 	${BTRFS_OPTIONS},subvol=@snapshots 	0 2
LABEL=${BOOT_PARTITION_LABEL}		/efi		vfat	defaults,noatime			0 2
tmpfs					/tmp		tmpfs	defaults,nosuid,nodev			0 0
EOF


# echo "tmpfs /tmp tmpfs defaults,nosuid,nodev 0 0" | tee /mnt/etc/fstab \
# > /dev/null
# echo "LABEL=${BOOT_PARTITION_LABEL} /boot vfat defaults 0 2" \
# | tee -a /mnt/etc/fstab > /dev/null
# echo "/dev/mapper/${VOLUME_GROUP_NAME}-root / btrfs defaults,noatime 0 1" | tee -a \
# /mnt/etc/fstab > /dev/null
# echo "/dev/mapper/${VOLUME_GROUP_NAME}-swap none swap defaults 0 1" | tee -a /mnt/etc/fstab \
# > /dev/null

# Unmounting the boot partition to write the label
umount "${DEVICE}"1
fatlabel "${DEVICE}"1 "${BOOT_PARTITION_LABEL}"
mount "${DEVICE}"1 /mnt/boot

# Chroot and run final configuration script:
cp -f chroot-script.sh /mnt/home/chroot-script.sh
chroot /mnt /bin/bash -c "/bin/sh /home/chroot-script.sh ${DEVICE} ${UEFI} ${ROOT_PASSWORD}"

# Change the root shell to bash (not mandatory)
# "-R" should change it in /mnt chroot
chsh -R /mnt -s /bin/bash
