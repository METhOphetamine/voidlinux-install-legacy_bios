#!/bin/sh
# Void installation. Calls other sub-scripts.
# Inputs:
# 1.: Device on which Void Linux must be installed, eg. "/dev/sda"
DEVICE="$1"
# 2.: Volume group name for the new LVM, eg "void".
VOLUME_GROUP_NAME="$2"
# 3.: New hostname to use.
HOSTNAME="$3"
# 4.: Boot partition label. Must be locally unique. No longer than 11 bytes.
BOOT_PARTITION_LABEL="$4"
# 5.: whether you want UEFI - "no" for legacy bios, "yes" for UEFI (UEFI is default)
UEFI="$5"
# 6.: The size of your boot partition which doesn't have to be that big but with 2G, you won't have to delete the old linux kernels that often
BOOT_PARTITION_SIZE="$6"
# 7. Size of swap space. Default is double of RAM size.
SWAP_SIZE="$7"

ROOT_PASSWORD="voidlinux" # unused I think

BTRFS_OPTIONS="rw,noatime,compress=zstd,discard=async"

check_config_and_sudo(){
    # -z means the string is zero lenght btw
    if [ -z "${DEVICE}" ]; then
	echo "You need to provide a device to erase and install void on it."
	exit 1
    fi
    if ! [ -e "${DEVICE}" ]; then
	echo "The specified drive does not exist!"
	exit 1
    fi
    
    if [ "$(id -u)" -ne 0 ]; then
	echo "Please run as root or with sudo"
	exit 1
    fi
}

set_defaults(){
    if [ "$HOSTNAME" = "" ]; then
	HOSTNAME="void";
    fi
    if [ "${LUKS_NAME}" = "" ]; then
	LUKS_NAME="${HOSTNAME}_crypt"
    fi

    if [ "$VOLUME_GROUP_NAME" = "" ]; then
	VOLUME_GROUP_NAME="${HOSTNAME}_LVM";
    fi

    if [ "$BOOT_PARTITION_LABEL" = "" ]; then
	BOOT_PARTITION_LABEL=$(echo "${HOSTNAME}-boot" | head -c 11)
    fi
    if [ "$BOOT_PARTITION_SIZE" = "" ]; then
	BOOT_PARTITION_SIZE="2G"
    fi

    if [ "$SWAP_SIZ"E = "" ]; then
	total_ram_in_kb=$(grep -i 'memtotal' /proc/meminfo | grep -o '[[:digit:]]*')
	double_of_ram_in_kb=$((2*total_ram_in_kb))
	SWAP_SIZE="${double_of_ram_in_kb}k"
    fi

}


lvm_setup(){
    # Encrypt the "/" (non-boot) partition
    # Using LUKS1, because LUKS2 is not recommended (https://docs.voidlinux.org/installation/guides/fde.html)
    # the "-" argument tells cryptsetup to read the password from stdin
    # echo "${ROOT_PASSWORD}" | cryptsetup luksFormat --type luks1 "${DEVICE}2" --key-file -
#EXCEPT IT DOESN'T WORK BECAUSE THE STDIN WAY DOES NOT SALT THE PASSWORD REEE
    cryptsetup luksFormat "${DEVICE}2"

    # Now we need to unlock the device
    # Similar thing
    # echo "${ROOT_PASSWORD}" | cryptsetup luksOpen "${DEVICE}2" --key-file - "${LUKS_NAME}"
    cryptsetup luksOpen "${DEVICE}2" "${LUKS_NAME}"

    # Once the LUKS container is opened, create the LVM volume group using that partition.
    vgcreate ${VOLUME_GROUP_NAME} /dev/mapper/${LUKS_NAME}

    # Next, logical volumes need to be created for the volume group. Unless specified I will choose 2x the RAM amount for swap and the rest for the rest of the filesystem
    total_ram_in_kb=$(grep -i 'memtotal' /proc/meminfo | grep -o '[[:digit:]]*')

    if [ "${SWAP_SIZE}" = "" ]; then
	double_of_ram_in_kb=$((2*total_ram_in_kb))
	SWAP_SIZE="${double_of_ram_in_kb}k"
    fi

    lvcreate --name swap -L "${SWAP_SIZE}" "${VOLUME_GROUP_NAME}"
    lvcreate --name root -l 100%FREE "${VOLUME_GROUP_NAME}"
    
    # Create filesystem on boot partition
    mkfs.fat -F32 "${DEVICE}1"

    # Create the filesystems on the logical volumes
    mkfs.btrfs -L root "/dev/mapper/${VOLUME_GROUP_NAME}-root"
    
    # we need to mount the root logical volume so that we can create btrfs subvolumes on it
    mount -o "$BTRFS_OPTIONS" "/dev/mapper/${VOLUME_GROUP_NAME}-root" /mnt
    
    # Create the subvolumes (root, home, snapshots):
    btrfs su cr /mnt/@
    btrfs su cr /mnt/@home
    btrfs su cr /mnt/@snapshots
    
    # Create swap on the swap LVM
    mkswap "/dev/mapper/${VOLUME_GROUP_NAME}-swap"
    swapon "/dev/mapper/${VOLUME_GROUP_NAME}-swap"
    
    # Unmount root LVM so that we can mount the root subvolume
    umount /mnt
    
    # Now we mount the subvolumes
    mount -o "${BTRFS_OPTIONS},subvol=@" "/dev/mapper/${VOLUME_GROUP_NAME}-root" /mnt
    mkdir /mnt/home
    mount -o "${BTRFS_OPTIONS},subvol=@home" "/dev/mapper/${VOLUME_GROUP_NAME}-root" /mnt/home
    mkdir /mnt/.snapshots
    mount -o "${BTRFS_OPTIONS},subvol=@snapshots" "/dev/mapper/${VOLUME_GROUP_NAME}-root" /mnt/.snapshots
    
    # While a key function and benefit of BtrFS is snapshots, there are certain directories that don’t need to be included in the snapshots, we will create some nested subvolumes for these specific directories
    mkdir -p /mnt/var/cache
    btrfs su cr /mnt/var/cache/xbps
    btrfs su cr /mnt/var/tmp
    btrfs su cr /mnt/srv

    # this is not need if we're doing the snapshot thing
    #mount /dev/mapper/"$VOLUME_GROUP_NAME"-root /mnt
    mkdir /mnt/boot
    mount "${DEVICE}1" /mnt/boot
}


main(){
    check_config_and_sudo
    set_defaults
    ./partitioning.sh "$DEVICE" "$UEFI" "$BOOT_PARTITION_SIZE"
    lvm_setup "$DEVICE" "$VOLUME_GROUP_NAME"
    ./base-install.sh "$DEVICE" "$VOLUME_GROUP_NAME" "$HOSTNAME" "$BOOT_PARTITION_LABEL" "$UEFI" "$ROOT_PASSWORD"
}

main

